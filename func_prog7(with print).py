def double_f(a):
	return a*2

def multiply(a, b, c):
	return a*b*c

def len_f(a):
	a = str(a)
	return len(a)

def even(a):
	#if a % 2 == 0:
	return (a % 2 == 0)

def empty(a):
	return a 

lst = [1, 2, 3, 4, 5, 6, 7, 8]
lst2 = [8, 7, 6, 5, 4, 3, 2, 1]
lst3 = [10, 20, 30, 40, 50, 60, 70, 80]

print("1. Удвоить каждый элемент коллекции: ")
print(list(map(double_f, lst)))

print("\n2. Найти произведение по-элементно элементов из трех коллекций: ")
print(list(map(multiply, lst, lst2, lst3)))

lst4 = [10, '20', 300, 'abcdefj', 'z', 6, 'abcd', '8kg']

print("\n3. Найти длину каждого элемента из коллекции: ")
print(list(map(len_f, lst4)))

print("\n4. Оставить только четные элементы коллекции: ")
print(list(filter(even, lst)))

lst5 = [1, '', (1,3), (), 5, [], [1, 8]]

print("\n5. Оставить только непустые элементы коллекции: ")
print(list(filter(empty, lst5)))

print("\n6. Есть три коллекции, нужно упаковать элементы тройками: ")
print(list(zip(lst, lst2, lst3)))

print("\n7. Есть две коллекции, нужно упаковать элементы двойками при этом элементы второй коллекции должны быть удвоенны: ")
print(list(zip(lst, map(double_f, lst2))))