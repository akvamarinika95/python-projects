operations = {  #создание словаря
	'+': lambda a, b: a + b, # каждый ключ ('+', '-', '/', '*') хранит свою лямбда - функцию (анонимная) с аргументами (a, b)
	'-': lambda a, b: a - b,
	'/': lambda a, b: a / b,
	'*': lambda a, b: a * b
}

while exit != 'n':
	try:
		a = int(input("Enter a: ")) #ввод числа а
		b = int(input("Enter b: ")) #ввод числа b
	except ValueError:  #обработка исключения, если пользователь вводит всё что угодно, кроме целого числа
		print("Error! Invalid input.")
	else:
		try:
			operator = input("Enter operation(+, -, /, *): ") #ввод оператора
			print(operations[operator](a, b)) #имя_словаря[ключ](аргументы фун-ии)
		except ZeroDivisionError: #обработка исключения, если пользователь захочет делить на 0
			print("Error! Can not be divided by zero!")
		except KeyError:    #обработка исключения, если пользователь введет оператор ("ключ"), а его нет в словаре
			print("Error! Not an operator (+, -, /, *)")
	exit = input("Do you want to continue (y / n)? ")
