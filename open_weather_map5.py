import urllib3
import sys
import json

def parsing(city_lst_id):
	weather = {}
	for id_city in city_lst_id:
		url = 'http://api.openweathermap.org/data/2.5/weather?id=%s&units=metric&appid=d6843ab8ee963f5d372296dfff62aed7' % id_city
		http = urllib3.PoolManager()
		response = http.request('GET', url)
		datastr = response.data.decode('utf-8')
		#print(datastr)
		data = json.loads(datastr)

		if data['name'] not in weather:
			weather[data['name']] = data['main']['temp']
	return weather

def display(parse_dict):
	for city, temp in sorted(parse_dict.items(), reverse = True, key=lambda item: item[1]):
		print('{1} : {0} C\N{SUPERSCRIPT ZERO}'.format(temp, city))

city_lst_id = sys.argv[1:]
parse_dict = parsing(city_lst_id)
#print(parse_dict)
display(parse_dict)

#Moscow (524901), Krasnodar (542420), Sochi (491422), Bodaybo (2026583), Khabarovsk (2022890), Vinogradovo(473537), Alupka(713514), Tokyo(1850147) Paris 20(6618626)
# Thomson Park(1882558), Seoul(1835848), Kaliningrad(554234), Ust-Ilimsk (2013952), Kirensk (2022083), Irkutsk(2023469), Listvyanka(2020743), London(5056033) Central Macedonia(6697801)
#524901 542420 491422 2026583 2022890 473537 713514 1850147 6618626 1882558 1835848 554234 2013952 2022083 2023469 2020743 5056033 6697801