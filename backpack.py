class Backpack(object):
	available_things = []
	"""docstring for Backpack"""
	def __init__(self, capacity, max_weight):
		self.max_weight = max_weight
		self.capacity = capacity

	def add_thing(self, subject):
		#weight, capacity = self.max_weight, self.capacity
		#print(self.max_weight, self.capacity)
		if not self.available_things:
			self.available_things.append(subject)
			self.max_weight -= subject.weight
			self.capacity -= subject.size
		elif (self.max_weight - subject.weight) > 0 and (self.capacity - subject.size) > 0:
			self.available_things.append(subject)
			self.max_weight -= subject.weight
			self.capacity -= subject.size
		else:
			print('There is no place for this item in the backpack! weight = {}, capacity = {}'. format(round(self.max_weight - subject.weight,1), self.capacity - subject.size))

	def del_thing(self, subject):
		if subject in self.available_things:
			self.available_things.remove(subject)
			self.max_weight += subject.weight
			self.capacity += subject.size
		else:
			print("This item is not found in the backpack!")

	def free_place(self):
		print('Free space in the backpack: {} kg, {} cells'.format(round(self.max_weight,1), self.capacity))

	def __str__(self):
		result = [''.join('\n{}: {} kg'.format(thing.name, thing.weight)) for thing in self.available_things]
		result.sort()
		return ' '.join(result)

class Thing(object):
	"""docstring for Thing"""
	def __init__(self, name, weight, size):
		self.name = name
		self.weight = weight
		self.size = size

	def __str__(self):
		return "%s: %s kg %s cells" % (self.name, self.weight, self.size)

backpack = Backpack(capacity = 55, max_weight = 25)

cup = Thing('cup', 0.35, 2)
laptop = Thing('laptop', 2.5, 7)
powerbank = Thing('powerbank', 1.45, 5)
book = Thing('book', 0.5, 3)
potato = Thing('potato', 3.5, 8)
canned_foods = Thing('canned foods', 1.9, 9)
lighter = Thing('lighter', 0.1, 1)
mat = Thing('mat', 0.7, 4)
boiler = Thing('boiler', 1.2, 6)
axe = Thing('axe', 1.8, 3)
spoon = Thing('spoon', 0.2, 1)
knife = Thing('knife', 0.2, 1)
sugar = Thing('sugar', 0.1, 1)
salt = Thing('salt', 0.2, 1)
tea = Thing('tea', 0.2, 1)
#print(axe)

backpack.add_thing(cup)
backpack.add_thing(laptop)
backpack.add_thing(powerbank)
backpack.add_thing(book)
backpack.add_thing(potato)
backpack.add_thing(canned_foods)
backpack.add_thing(lighter)
backpack.add_thing(mat)
backpack.add_thing(boiler)
backpack.add_thing(axe)
backpack.add_thing(spoon)
backpack.add_thing(knife)
backpack.add_thing(sugar)
backpack.add_thing(salt)
backpack.add_thing(tea)

print(backpack)
backpack.free_place()

backpack.del_thing(laptop)
print(laptop)
backpack.free_place()

backpack.del_thing(powerbank)
print(powerbank)
backpack.free_place()
print(backpack)

backpack.add_thing(laptop)
backpack.add_thing(laptop)