from tkinter import* 
from tkinter import colorchooser 
from random import*
import math
window = Tk()  #создание окна tkinter
w = 600
h = 600

canv = Canvas(window, width = w, height = h, bg = 'white') # создать холст в окне window
canv.pack(fill = BOTH, expand = 1) # указание расположить холст внутри окна; размер - максимально возможный в обе стороны

circles = []
field = []

def get_circle_random():
	x = randint(-w/10, w) # окр-ти слева и сверху
	y = randint(-h/10, h)
	r = randint(1, w/5)
	return [x,y,r]

def get_circle():
	try:
		x = float(input("Enter x: "))
		y = float(input("Enter y: "))
		r = float(input("Enter r: "))
	except ValueError:
		print("Error! Invalid input.")
	else:
		if (x <= w and x >= 0 ) and (y <= h and y >= 0) and r > 0:
			return [x,y,r] 
		else:
			print("Exceeds the size of the field ", w, " x ", h)
			return False

def check_list(new_circle):
	x2 = new_circle[0]
	y2 = new_circle[1]
	r2 = new_circle[2]
	
	if (w < x2 + r2) or (h < y2 + r2) or (0 > x2 - r2) or (0 > y2 - r2):
		print("The circle goes beyond the field!")
	else:
		if not field:
				return True
		for i in range(len(field)):
			x1 = field[i][0]
			y1 = field[i][1]
			r1 = field[i][2]
			
			d = math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
			if (d > r1 - r2 and d < r1 + r2) or (d <= r1 - r2) or (d == 0): 
				return False
		return True 

def paint_axes():
	for x in range(40,600,20):
		canv.create_line(x,40,x,580,fill='lightgray')
		canv.create_text(x,20,text=x,font = 'Arial 8')
	for y in range(40,600,20):
		canv.create_line(40,y,580,y,fill='lightgray')
		canv.create_text(20,y,text=y,font = 'Arial 8')
	canv.create_line(40,40,580,40,width=2,arrow='last')
	canv.create_line(40,40,40,580,width=2,arrow='last')
	
def paint_circle(field):
	colors = choice(['blue', 'green', 'red', 'cyan', 'magenta', 'purple', 'orange', 'yellow', 'pink', '#66ff33', '#3399ff','#00cc99'])
	w = 1
	x, y, r = 0, 0, 0

	for i in range(len(field)):
		x = field[i][0]
		y = field[i][1]
		r = field[i][2]
	return canv.create_oval(x-r,y-r,x+r,y+r,width=w, fill = colors)
	

diapason = 0
while diapason < 2000:
	paint_axes()
	new_circle = get_circle_random() # OR input  new_circle = get_circle()
	circles.append(new_circle)
	try:
		if check_list(new_circle):	
			field.append(new_circle)
			paint_circle(field)
	except TypeError:
		print("TypeError: 'NoneType' object is not subscriptable")
	else:
	    diapason +=1
	window.update()
