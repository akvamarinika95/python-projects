def my_zip(a, b):
	return (a, b)

def my_zip2(*args):
	return tuple(args)

lst = [1, 2, 3, 4, 5, 6, 7, 8]
lst2 = [8, 7, 6, 5, 4, 3, 2, 1]
lst3 = [10, 20, 30, 40, 50, 60, 70, 80]

print(list(map(my_zip, lst, lst2))) 

print(list(map(lambda a, b: (a, b),  lst, lst2)))

print(list(map(my_zip2, lst, lst2, lst3)))

zipper = (list(map(lambda *args: (tuple(args)), lst, lst2, lst3)))
print(zipper)
