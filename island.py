def read_file(file_name):
	lst = []
	try:
		with open(file_name) as f:
			lst = f.readlines()
			lst = [int(elem.rstrip('\n')) for elem in lst]
			return lst 
	except IOError:
		print("An IOError has occurred!")

def analytical_search_for_water(island):
	water = 0
	index_left = 0
	index_right = len(island) - 1
	max_left = 0
	max_right = 0

	while (index_left < index_right):
		if island[index_left] > max_left:
			max_left = island[index_left]

		if island[index_right] > max_right:
			max_right = island[index_right]

		if max_left >= max_right:
			water += max_right - island[index_right]
			index_right -= 1

		elif max_right >= max_left:
			water += max_left - island[index_left] 
			index_left += 1
	return water

def test_check():
	import random
	lst = [random.randint(1, 10) for i in range(20)]
	return lst

#функция - "велосипед":
def graphic_island(lst):
	count = 0
	n = max(lst)
	m = len(lst)
	lst2 = [[1]*m for i in range(n)]
	water = list(map(lambda elem: n - elem, lst))
	for i in range(n):      
		for j in range(m):
			if i < water[j]:
				lst2[i][j] = '*'
				count += 1


	for i in range(n): 
		if lst2[i][0] == '*':
			lst2[i][0] = 0
			count -= 1
	          
		if lst2[i][m-1] == '*':
			lst2[i][m-1] = 0
			count -= 1
	
		for ind_l in range(m-1): 	
			if (lst2[i][ind_l] == 0 and lst2[i][ind_l+1] == '*'):
				lst2[i][ind_l+1] = 0
				count -= 1
			if lst2[i][ind_l] == 1:
				break
	
		for ind_r in range(m-1, 0, -1):
			if (lst2[i][ind_r] == 0 and lst2[i][ind_r-1] == '*'):
				lst2[i][ind_r-1] = 0
				count -= 1

			if lst2[i][ind_r] == 1:
				break

	print('water = ', count,  ' (\'bike\'-function)')
	for row in lst2:
		print(' '.join([str(elem) for elem in row]))
	

island = read_file('input.txt')
print('\n * - water, 1 - block, 0 - empty: ')
print('\nwater = ',analytical_search_for_water(island), ' (analytical-function)')
graphic_island(island)


test_list = test_check()
print('\nwater = ', analytical_search_for_water(test_list), ' (analytical-function)')
graphic_island(test_list)

test_list = test_check()
print('\nwater = ', analytical_search_for_water(test_list), ' (analytical-function)')
graphic_island(test_list)