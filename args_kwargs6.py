# без комментариев :) 
def added(*args, **kwargs):
	if not args or not kwargs:
		print("Args or kwargs is empty! Enter args(kwargs).\n")
	else:
		print (args)
		print (kwargs)

def min_num(*args):
	if not args:
		print("Args is empty! Enter args.")
	else:
		num = args[0]
		for arg in args:
			if arg < num:
				num = arg
		print (args)
		print(("Minimum: {0} \n").format(num))

def min_255(*args):
	if not args:
		print("Args is empty! Enter args.")
	else:
		num = args[0]
		for arg in args:
			#print(j)
			if arg >= 0 and arg <= 255:
				if arg < num:
					num = arg
			else:
				print("Number is not in the range from 0 to 255.")
		print (args)
		print(("Minimum: {0}").format(num))


a = added()
b = min_num(22, 3, 4, -1, 100, 68, 97, -15, 67, 9, 235, 1000, 8, -8)
c = min_255(20, 8, 10, 171, 50 , 5, 130, 255, 48, 79, 150)

