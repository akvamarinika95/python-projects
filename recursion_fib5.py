#без комментариев :)
def fib(n):

	fib1 = 1
	fib2 = 1
	
	i = 2
	while i < n:
		fib_sum = fib2 + fib1
		fib1 = fib2
		fib2 = fib_sum 
		i +=1
	return fib_sum
	
dictionary = {0: 0, 1: 1}
def fib_rec(n):
	if n in dictionary:
		 return dictionary[n]
	dictionary[n] = fib_rec(n - 1) + fib_rec(n - 2)
	return dictionary[n]
	
n = int(input("The value of which element of Fibonacci do you want to know?: "))
a = fib(n)
print("Function while: ", a)
b = fib_rec(n)
print("Function recursion: ", b) 
