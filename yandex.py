import json 
count_lang = {}

def read_json(file_name):
	f = open(file_name)
	lines = f.readlines()
	json_string = ''.join(lines)
	languages = json.loads(json_string)
	return languages
#print(languages)
def number_of_translation_languages(count_lang, languages):
	for dir in languages['dirs']:
		lang, lang_translate = dir.split('-')
			#print(lang)
		count_lang[lang] = count_lang.get(lang, 0) + 1
	return count_lang


def replacement_keys_on_russian(count_lang, languages):
	for key_lang in count_lang:
		for lang, name in languages['langs'].items():
			#print(lang, val)
			if lang in count_lang:
				count_lang[name] = count_lang.pop(lang)
	return count_lang

translate_lang = dict()
def babylon(translate_lang, languages):
	for key_dir in languages['dirs']:
		lang, transl = key_dir.split('-')
		if lang in translate_lang:
			translate_lang[lang].append(transl)
		else:
			translate_lang[lang] = [transl]
	#translate_lang.sort(key=lambda t: len(t[1]), reverse = True)
	return translate_lang

def display(translate_lang):
	for lang, translate in sorted(translate_lang.items(), key=lambda item: len(item[1] )):
		print(lang,': ', ', '.join(translate))		

languages = read_json('yandex.json')
print(number_of_translation_languages(count_lang, languages), '\n')
print(replacement_keys_on_russian(count_lang, languages), '\n')
babylon(translate_lang, languages)
print(translate_lang)
display(translate_lang)