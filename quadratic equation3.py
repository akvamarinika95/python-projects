import math # подключение библиотеки, для извлечения корня из чисел типа float 
import cmath # подключение библиотеки, для извлечения корня из комплексных чисел 

def input_user(text, flag): # функция для ввода и проверки пользователя (корректен ли ввод) 
	coeff = None #коэфф-т пуст
	while coeff is None : #выполняется цикл, пока пользователь не введет допустимое значение для коэфф-та
		try:
			coeff = float(input(text)) #ввод коэффициента ур-ия пользователем
			if not flag and abs(coeff) == 0.0:  #обработка события если a == 0, (условие if выполнится тогда, когда флаг для коэфф-та "a" примет значение True (not False == True)
				print("The coefficient a is not zero!")
				coeff = None
		except ValueError: #обработка исключения, если пользователь вводит всё что угодно, кроме float - числа
			print("Error! No valid number entered!")

	return coeff

while exit != 'n':
	print("Enter coefficients for quadratic equation ax\N{SUPERSCRIPT TWO} + bx + c = 0")
	a = input_user("Enter a: ", False) #ввод коэффициента а через вызов фун-ии input_user()
	b = input_user("Enter b: ", True ) #ввод коэффициента b через вызов фун-ии input_user()
	c = input_user("Enter c: ", True ) #ввод коэффициента c через вызов фун-ии input_user()

	x2 = None # пустая перем-ая х2, 

	discriminant = (b ** 2)-(4 * a * c)
	if discriminant == 0: #если D = 0, то уравнение имеет 1 корень
		x1 = -(b / (2 * a))
	elif discriminant > 0: #если D > 0, то уравнение имеет 2 действительных корня
		disc_root = math.sqrt(discriminant) # вычисляем корень из дискриминанта
	else:  # discriminant < 0, то  то уравнение имеет 2 комплексных корня
		disc_root = cmath.sqrt(discriminant)

	x1 = ( -b + disc_root) / (2 * a) # x1 - корень уравнения
	x2 = ( -b - disc_root) / (2 * a) # x2 - корень уравнения


	y = ("{0}x\N{SUPERSCRIPT TWO} + {1}x + {2} = 0"  #с помощью метода format делаем подстановку данных в строку '{0}, {1}, {2}'.format('a', 'b', 'c')
	            "\n x1 = {3}").format(a, b, c, x1)

	if x2 is not None: # проверка, если корень х2 не пуст, то прибавляем строку к основной строке
		y += "   x2 = {0}".format(x2)

	print(y) # и выводим результат
	
	exit = input("Do you want to continue (y / n)? ")