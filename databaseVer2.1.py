import time
import os
import matplotlib.pyplot as plt
from functools import reduce

for_print = ['name', 'second_name', 'post', 'age', 'gender', 'salary_in_hour', 'coefficient' ]

def graph(sec, count):
	line = plt.plot(sec, count)
	plt.setp(line, color='r', linewidth=2.0)
	plt.xlabel('Seconds')
	plt.ylabel('Count')
	plt.show()
	#save('pic_10_1', fmt='png')

class Employee(object):
	
	def __init__(self, name = '', second_name = '', post = '', age = 0, gender = '', salary_in_hour = 0, coefficient = 0.0):
		self.name = name
		self.second_name = second_name
		self.post = post
		self.age = age
		self.gender = gender
		self.salary_in_hour = salary_in_hour
		self.coefficient = coefficient
	
	#рассчет з/п за n-дней
	def calc_salary(self):		
		days =  int(input('Enter count days: '))
		salary = self.salary_in_hour * 8 * self.coefficient * days
		return '\nsalary for {} days: {}'.format(days, salary)

	#рассчет кол-ва лет до пенсии
	def retirement_years(self):
		if self.gender == 'M' or self.gender == 'm':
			years = 65 - self.age
			return '\nyears left until retirement: {} years, if not add =)'.format(years)
		elif self.gender == 'W' or self.gender == 'w':
			years = 63 - self.age
			return '\nyears left until retirement: {} years, if not add =)'.format(years)
		else:
			print("Gender not determined!")

class DataBase(object):

	def __init__(self):	
		self.dict_employees = {}
		self.empCount = 0
		self.lst_employees = []
		self.hashs_lst = []
		
	def hash_obj(self, obj):
		h_name = hash(obj.name)
		h_second_name = hash(obj.second_name)
		hash_obj = [h_name, h_second_name, obj.age]
		#print(hash_obj)
		if not self.hashs_lst:
			self.hashs_lst.append(hash_obj)
			return obj
		else:
			for lst in self.hashs_lst:
					if lst[0] != hash_obj[0] and lst[1] != hash_obj[1] and lst[2] != hash_obj[2]:
						self.hashs_lst.append(hash_obj)
						return obj


	def add_employee(self, obj):

		self.dict_employees = {'name': obj.name,
					'second_name': obj.second_name,
					'post': obj.post,
					'age': obj.age,
					'gender': obj.gender,
					'salary_in_hour': obj.salary_in_hour,
					'coefficient': obj.coefficient
					}

		self.lst_employees.append(self.dict_employees.copy())
		self.empCount += 1
	

	def _read(self, file_name):
		name1 = []
		try:
			with open(file_name) as f:
				name1 = f.readlines()
				name1 = [line.rstrip('\n') for line in name1]		
				return name1
		except IOError:
				print("An IOError has occurred!")


	def seed_db_automatically(self, obj):
		import random
		names = base._read('names.txt')
		surnames = base._read('names.txt')
		#names = ['Aleksandr', 'Alex', 'Pavel']
		#surnames = ['Ivanov', 'Petrov', 'Sidorov']
		posts = ['Senior Engineer', 'Senior Programmer', 'System Administrator', 'System Engineer', 'Software Developer',
				'Software Tester', 'Senior Web Developer', 'Junior Software Engineer', 'Middle Programmer', 'Team Leader',
				'Project Manager', 'Software Architect', 'Technical Support Engineer', 'Chief Technology Officer',
				'Chief Information Officer', 'Network Administrator', 'Senior Network Engineer', 'Help Desk Specialist', 
				'Senior Support Specialist', 'Database Administrator', 'Security Specialist', 'Front End Developer', 'Webmaster']

		gender = ['M', 'W']  # Уж кому как повезет =)

		obj.name = random.choice(names)
		obj.second_name = random.choice(surnames)
		obj.post = random.choice(posts)
		obj.gender = random.choice(gender)	
		obj.age = random.randint(25, 50)
		obj.salary_in_hour = random.randint(1000, 5000)
		obj.coefficient = random.randint(5, 15)
		return obj
	

	def delete__employee(self):
		print('Remove employee => ')		#магическим образом интерпретатор кладет значения переменных name и second_name в кортеж 
		name = input('Enter name for deletion: '), 	#не нашла ответ зачем он это делает (с age такого не происходит, хотя тоже строка) 
		second_name = input('Enter second name for deletion: '),
		age = input('Enter age for deletion: ')

		found_employee = None
		for dict_emp in self.lst_employees:
			#print(name[0])
			#print(second_name[0])
			#print(age)
			if (name[0] == dict_emp['name'] and 
				second_name[0] == dict_emp['second_name'] and
				age == str(dict_emp['age'])):
				found_employee = dict_emp
				break	

		if found_employee != None:
			print('****Delete****')
			print(found_employee)

			self.lst_employees.remove(found_employee)
			self.empCount -= 1
			base.save_db('workers.txt')
		

	def display_employee(self):
		print('Datebase has ' + str(self.empCount) + ' workers: ')

		for dict_emp in self.lst_employees:
			print('----------------------------------------')
			for key in for_print:
				pad = ' ' * (len('salary_in_hour') - len(key))
				print (key, pad, dict_emp[key])


	def save_db(self, file_name):
		try:
			f = open(file_name, 'w')
			f.write(str(self.lst_employees))
			
		except IOError:
			print("An IOError has occurred!")
		finally:
			f.close()


	def load_db(self, file_name):
		#if (os.start(file_name).st_size == 0):		#в файл по какой-то причине не записывается пустой список '[]'
			#with open(file_name) as f:					#пыталась написать "костыль".. не знаю, как это исправить :/ 
				#f.write('[]')
		#else:
		try:
			with open(file_name) as f:
				self.lst_employees = eval(f.readline())
			#print(self.lst_employees)
		except IOError:
				print("An IOError has occurred!")
	
		self.empCount = 0
		for dict_emp in self.lst_employees:
			self.empCount +=1

	#суммарная з/п
	def summ_salary(self):
		sal_hour = [] 
		try:
			for dict_emp in self.lst_employees:
				sal_hour.append(dict_emp['salary_in_hour'])
			summ = reduce(lambda a, b: a + b, sal_hour) 
		except TypeError :
			print("DataBase is empty!")
		else:
			return '\ntotal salary: {}'.format(summ)

	#список работников, которым осталось до пенсии n-лет
	def lst_employee_retirement(self):
		lst_age = []
		if not self.lst_employees:
			_main()	
		else:
			years = int(input('The list of workers, who are left until retirement N-years. Enter N: '))
			for dict_emp in self.lst_employees:
				if (dict_emp['gender'] == 'M' or  dict_emp['gender'] == 'm'):
					if years == (65 - dict_emp['age']):
						lst_age.append(dict_emp)
				else:
					if years == (63 - dict_emp['age']):
						lst_age.append(dict_emp)

			for dict_emp in lst_age:
				print('----------------------------------------')
				for key in for_print:
					pad = ' ' * (len('salary_in_hour') - len(key))
					print (key, pad, dict_emp[key])
		

base = DataBase()
def _main():
	exit = 'y'		
	base.load_db('workers.txt')		#если файл совсем пуст интерпретатор ругается, что он ожидал увидить "[]" в файле:/

	while exit:
		regime = input('Enter mode (1 - Manual entry(Add), 2 - Remove, 3 - Printout, 4 - Auto fill database, 5 - summ_salary & retirements): ')
		if regime == '1':
			emp = Employee(name = input('Enter name: '),
						second_name = input('Enter second name: '),
						post = input('Enter post: '),
						age = int(input('Enter age: ')),
						gender = input('Enter gender (M or W): '),
						salary_in_hour = int(input('Enter salary (in hour): ')),
						coefficient = float(input('Enter boost factor: ')))
			print(emp.calc_salary())
			print(emp.retirement_years())
			base.hash_obj(emp)
			base.add_employee(emp)
			base.save_db('workers.txt')

		elif regime == '2':
			base.delete__employee()
			base.save_db('workers.txt')

		elif regime == '3':
			base.display_employee()

		elif regime == '4':
			count = int(input('How many workers generate automatic? '))

			start_time = time.time()
			seconds = []
			counter = []

			n = 0
			while n < count:
				emp1 = Employee()
				base.seed_db_automatically(emp1)
				base.hash_obj(emp1)
				base.add_employee(emp1)
				base.save_db('workers.txt')

				seconds.append(round((time.time() - start_time), 3))
				counter.append(base.empCount)
				n +=1

			base.display_employee()
			graph(seconds, counter)

		elif regime == '5':
			print(base.summ_salary())
			base.lst_employee_retirement()
			

		exit = input("\nDo you want to continue (y / n)? ")
		if exit == 'n':
			print("Have a nice day!))")	
			break

#if __name__ == '__main__':
_main()	
exit()


