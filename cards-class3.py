import random

suits_symbols  = {'Spades':'♠', 'Diamonds':'♦', 'Hearts': '♥', 'Clubs': '♣'} 
card_values = {
			'Ace': 14, 
			'2': 2,
			'3': 3,
			'4': 4,
			'5': 5,
			'6': 6,
			'7': 7,
			'8': 8,
			'9': 9,
			'10': 10,
			'Jack': 11,
			'Queen': 12,
			'King': 13
	}


class Cards(object):
	"""docstring for Cards"""
	suits_significance = {'Spades': 1, 'Diamonds': 3, 'Hearts': 4, 'Clubs': 2} 

	def __init__(self, name, values):
		self.suit = name
		self.val = str(values)
		
	def __str__(self):

		y = 10
		field = [[] for i in range(y)]

		if (self.val == '10'):
			space = ''
			rank  = self.val
		elif (self.val in card_values):
			space = ' '   
			rank  = self.val[0]

		field[0].append('┌──────────┐')
		field[1].append('│{}{}        │'.format(rank, space))
		field[2].append('│          │')
		field[3].append('│          │')
		field[4].append('│    {}     │'.format(suits_symbols[self.suit]))
		field[5].append('│          │')
		field[6].append('│          │')
		field[7].append('│        {}{}│'.format(space, rank))
		field[8].append('└──────────┘')

		result = [''.join(f) for f in field]

		return '\n'.join(result)

	def hidden_card(self, *args):
		hidden = ['┌──────────┐'] + ['│☾★☾★☾★☾★☾★│'] * 7 + ['└──────────┘']
		result = [''.join(line) for line in hidden]
		#print(result)
		return '\n'.join(result)

	def compare(self, card_comp):
		#Червы > Бубновая > Треф > Пиковая
		if (card_values[self.val] > card_values[card_comp.val]): 
			return '+1'
		elif (card_values[self.val] == card_values[card_comp.val]):
				if (suits_significance[self.suit] > suits_significance[card_comp.suit]):
						return '+1'
				elif (suits_significance[self.suit] < suits_significance[card_comp.suit]):
						return -1
				else:
						return 0
		else:
			return -1


class Deck(object):
	"""docstring for Deck"""
	cards_deck = []
	def __init__( self ):
			self.cards_deck = [Cards( suit, val) for suit in suits_symbols for val in card_values]
			#print(self.cards_deck)
			random.shuffle( self.cards_deck)

	def distribution_of_cards(self):
		return self.cards_deck.pop()

class Players(object):
	"""docstring for Players"""
	def __init__(self, name):
		self.name_player = name
		self.cards_player = []
		self.score = 0
		
	def add_card(self, card):
		self.cards_player.append(card)
		
	def scoring(self, num):
		self.score += int(num)

	def __str__(self):
		#print(" ♥ ♦ ♣ ♠ {} CARDS: ♠  ♣  ♦  ♥ ".format(self.name_player.upper()))
		#for card in self.cards_player:
			#print(card)
		s = "{} score: {}\n".format(self.name_player, str(self.score))
		return s

	def del_of_cards(self, card):
		if card in cards_player:
			self.cards_player.remove(card)
		return self.cards_player



deck = Deck()
player1 = Players('Player')
comp = Players('Computer')


for i in range(6):
	player1.add_card(deck.distribution_of_cards())
	comp.add_card(deck.distribution_of_cards())

print(player1)

for card in comp.cards_player:
	print(card)

#print(" ♥ ♦ ♣ ♠ DEALER CARDS: ♠  ♣  ♦  ♥ ")
#for card in comp.cards_player:
	#print( card.hidden_card())

print("-----------------------------test1-----------------------------------------")

player_random_card = random.choice(player1.cards_player)
print('PLAYER:')
print(player_random_card)
comp__random_card = random.choice(comp.cards_player)
print('DEALER:\n')
print(comp__random_card)

result = player_random_card.compare(comp__random_card)
print(result,'\n')

player1.scoring(result)
print(player1)
print("-----------------------------test2----------------------------------------")

player_random_card = random.choice(player1.cards_player)
print('PLAYER:')
print(player_random_card)
comp__random_card = random.choice(comp.cards_player)
print('DEALER:')
print(comp__random_card)

result = player_random_card.compare(comp__random_card)
print(result,'\n')

player1.scoring(result)
print(player1)

#card1 = Cards('Hearts', '10')
#print(card1)
#comp_card = Cards('Hearts', '8')
#print(card1.compare(comp_card))

#print(c.hidden_card())
