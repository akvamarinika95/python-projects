class Element(object):
	"""docstring for Element"""
	def __init__(self, text='', subelement=''):
		self.text = text
		self.subelement = subelement

	def __str__(self):
		class_name = self.__class__.__name__.lower() 
		s = '<' + class_name + '>\n'
		if (self.text):
			s +=  '{} \n'.format(self.text)
		if (self.subelement):
			s += '{}\n'.format(self.subelement)
		s += '</{}>'.format(class_name)
		return s 

class P(Element):
	"""docstring for P"""
	def __init__(self, text='', subelement=''):
		super(P, self).__init__(text)

	def __str__(self):
		return super().__str__()
		
class Body(Element):
	 
	def __init__(self, text='', subelement=''):
		super(Body, self).__init__(text, subelement)

	def __str__(self):
		return super().__str__()

class Html(Element):
	"""docstring for Html"""
	def __init__(self, subelement='', text= ''):
		super().__init__(subelement)

	def __str__(self):
		s = '\n' + super(Html, self).__str__() + '\n'
		return super().__str__()


para = P(text="this is some body text")
doc_body = Body(text="This is the body", subelement=para)
doc = Html(subelement=doc_body)
print(doc)
#print(para)
#print(doc_body)