
class Vector2D(object):
	def __init__(self, *value_v):
		self.value_v = tuple(value_v)

	def __str__(self):
		if len(self.value_v) == 1:
			return 'Scalar product scalar product of two-vectors: {}'.format(*self.value_v)
		else:
			return 'Vector_2D{}'.format(self.value_v)

	def __add__(self, vector2):
		return Vector2D(self.value_v[0]+vector2.value_v[0], self.value_v[1]+vector2.value_v[1])
		
	def __sub__(self, vector2):
		return Vector2D(self.value_v[0]-vector2.value_v[0], self.value_v[1]-vector2.value_v[1])

	def __mul__(self, vector2):
		return Vector2D(sum((self.value_v[0]*vector2.value_v[0], self.value_v[1]*vector2.value_v[1])))
		

class VectorN_D(Vector2D):
	def __init__(self, *value_v):
		Vector2D.__init__(self, *value_v)

	def __str__(self):
		if len(self.value_v) == 1:
			return 'Scalar product of n-vectors: {}'.format(*self.value_v)
		else:
			return 'Vector_{}D{}'.format(len(self.value_v), self.value_v)
		
	def __add__(self, vector2):
		return VectorN_D(*[val_1 + val_2 for val_1, val_2 in zip(self.value_v, vector2.value_v)])
		
	def __sub__(self, vector2):
		return VectorN_D(*[val_1 - val_2 for val_1, val_2 in zip(self.value_v, vector2.value_v)])

	def __mul__(self, vector2):
		return VectorN_D(*[sum(val_1 * val_2 for val_1, val_2 in zip(self.value_v, vector2.value_v))])

vector1 = Vector2D(1, 2)
vector2 = Vector2D(4, 8)
print(vector1 + vector2)
print(vector1 - vector2)
print(vector1 * vector2, '\n')

vector3 = VectorN_D(1, 2, 10, 12)
vector4 = VectorN_D(4, 8, 14, 17)
print(vector3 + vector4)
print(vector3 - vector4)
print(vector3 * vector4, '\n')

vector3 = VectorN_D(10, 20, 30)
vector4 = VectorN_D(5, 8, 10)
print(vector3 + vector4)
print(vector3 - vector4)
print(vector3 * vector4)