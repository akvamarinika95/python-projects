
#"пузырек":
def bubble_sort(height, new):
	height.append(new) 
	for j in range(0,len(height)):   
		for i in range(0, len(height)-j-1): 
			if height[i] < height[i+1]:
				temp = height[i]
				height[i] = height[i+1]
				height[i+1] = temp
	print (height)

#вставка с помощью insert()
def insert1(lst, new):
	for i in range(0,len(lst)):
		if lst[i] < new:
			lst.insert(i, new)
			break
		elif new < lst[len(lst)-1]: 
			lst.insert(len(lst), new)
	print(lst)

# sort()
def libr_sort(lst, new):
	lst.append(new)
	lst.sort(reverse=True)
	print(lst)

#попытка сделать вставку с помощью срезов(
def slices_insert(lst, new):
	for i in range(0,len(lst)):
		#print(i)
		if lst[i] < new:
			 return lst[:i] + [new] + lst[i:]
		return lst + [new] 

# ещё одна неудачная попытка сделать вставку с помощью срезов(
def min_max_insert(lst, new):
	max = len(lst)
	min = 0 
	while max - min > 0:
		m = (min + max) // 2 # отрезок // 2
		if lst[m] < new:
			max = m
		else:
			min = m + 1
	return lst[:max] + [new] + lst[max:]

#поиск эл-та "new" с конца 
def search(height, new):
	for i in range(len(height)-1, -1, -1):
		#print(i)
		if height[i] == new:
			num = len(height) - i
			print("Number from the end: ", num)
			break 


height1 = [170, 165, 163, 163, 160]
print("Source list: ", height1)
height = list(height1)

while exit != 'n':
	try:
		new = int(input("Enter height: "))
	except ValueError:
		print("Error! No valid number entered!")
	else:
		num_func = None
		while num_func is None:
			num_func = (input("Enter № function 1; 2; or 3 (1 - bubble sort; 2 - insert() 3 - library sort()): "))
			
			if num_func == '1':
				bubble_sort(height, new)
				search(height, new)
			elif num_func == '2':
				insert1(height, new)
				search(height, new)
			elif num_func == '3':
				libr_sort(height, new)
				search(height, new)
			#elif num_func == '4':
				#print(slices_insert(height, new))
				#search(height, new)
			#elif num_func == '5':
				#print(min_max_insert(height, new))
				#search(height, new)
			else: 
				print("Function with this number was not found!")
				num_func = None
		
	
	exit = input("Do you want to continue (y / n)? \n")

