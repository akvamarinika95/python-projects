import random
import time
import json
import re

class Client(object):
	def __init__(self, passport, full_name):
		self.passport = passport
		self.full_name = full_name

	def hand_over_a_ticket(self):
		pass

class Purchase(object):
	def __init__(self):
		self.ticket_obj = Ticket()

	def money_request(self):
		money = int(input('Deposit money for the ticket. Ticket price ${}: '.format(self.ticket_obj.cost) ))
		if money == self.ticket_obj.cost:
			print('\nThank you for your purchase!')
			print('\nYour ticket :', self.ticket_obj, sep='\n')
			self.ticket_obj.status_ticket = 0
			return self.ticket_obj
		elif money > self.ticket_obj.cost:
			change = money - self.ticket_obj.cost
			print('\nYour change ${}: '.format(change))
			print('\nThank you for your purchase!')
			print('\nYour ticket :', self.ticket_obj, sep='\n')
			self.ticket_obj.status_ticket = 0
			return self.ticket_obj
		else:
			print('\nInsufficient funds!')


	def free_tickets_check(self, lst_tickets):
		for ticket_obj in lst_tickets:
		 	if (hash(self.ticket_obj.destination) == hash(ticket_obj.destination) and 
		 		hash(self.ticket_obj.date) == hash(ticket_obj.date) and 
		 		self.ticket_obj.seat == ticket_obj.seat and ticket_obj.status_ticket):
		 		self.ticket_obj = ticket_obj
		 		#print('\nYour ticket :', self.ticket_obj, sep='\n')
		 		return self.ticket_obj
		
		print('Sorry, all tickets to this destination have been sold.')
		return 0
		

class ElectronicCashDesk(object):
	def __init__(self, status=True):
		self.status = status
		self.ticket_obj = Ticket()

	def passport(self, db_clients, passport, full_name):
		if re.match(r"\d{10}", passport) and re.match(r"[A-Za-z]+\s[A-Za-z]+\s[A-Za-z]+", full_name):
			print('Successfully!')
			db_clients.append({'passport' : passport, 'full_name' : full_name})
			return db_clients
		else:
			raise TypeError ('Incorrect data entered!')

	def sell_a_ticket(self, lst_tickets):
		print('\nAvailable flights:')
		flights = set([ticket_obj.destination for ticket_obj in lst_tickets])
		flights_dict = {}
		for i, fl in enumerate(sorted(flights)):
			flights_dict[i+1] = fl
			print(i+1, fl)
		#print(flights_dict)
		dest_key = int(input('Which country do you want to fly to? Enter number: '))

		if dest_key in flights_dict:
			self.ticket_obj.destination = flights_dict[dest_key]
			print('\nAvailable date (MM:DD:YEAR): ')
			date = set([ticket_obj.date for ticket_obj in lst_tickets if hash(flights_dict[dest_key]) == hash(ticket_obj.destination)])
			date_dict = {}
			for i, dat in enumerate(sorted(date)):
				date_dict[i+1] = dat
				print(i+1, dat)
		else:
			raise UnboundLocalError ('Incorrect data entered!')

			dat_key = int(input('\nChoose departure date & time. Enter number: '))
			if dat_key in date_dict:
				self.ticket_obj.date = date_dict[dat_key]
				print('\nAvailable seats:')
				seats = set([ticket_obj.seat for ticket_obj in lst_tickets if hash(flights_dict[dest_key]) == hash(ticket_obj.destination) and date_dict[dat_key] == ticket_obj.date])
				seat_dict = {}
				for i, seat in enumerate(sorted(seats)):
					seat_dict[i+1] = seat
					print(i+1, seat)
			else:
				raise UnboundLocalError ('Incorrect data entered!')

				seat_key = int(input('\nChoose a seat. Enter number: '))
				if seat_key in seat_dict:
					self.ticket_obj.seat = seat_dict[seat_key]
				else:
					raise UnboundLocalError ('Incorrect data entered!')
		return self.ticket_obj
				
		
	def refund(self, arg):
		pass

	def shift_opening(self, arg):
		pass

	def shift_closure(self, arg):
		pass

	def send_report(self, arg):
		pass

	def prepare_a_report(self, arg):
		pass

	def close_for_a_break(self, arg):
		pass

	def checking_the_open_cash_desk(self, arg):
		pass

class Ticket(object):
	def __init__(self, number=000000, date='00/00/0000 00:00 AM', destination='city', seat=000, cost=0, status_ticket=True, buyer =''):
		self.number = number
		self.date = date
		self.destination = destination
		self.seat = seat
		self.cost = cost
		self.status_ticket = status_ticket
		self.buyer = buyer

	def __str__(self):
		result = ''
		for key in self.__dict__:
			#print(self.__dict__[key])
			pad = ' ' * (len('status_ticket ') - len(key))
			line = ''.join((key, pad, str(self.__dict__[key]))) 
			result = result.lstrip() + '\n' + ''.join(line)
			#print(result)
		return result


class DataBase(object):
	lst_tickets = []
	lst_clients = []

	def display_db(self):
		print('Datebase has ' + str(len(self.lst_tickets)) + ' tickets: ')
		for ticket_obj in self.lst_tickets:
			print('----------------------------------------')
			print(ticket_obj)

	@staticmethod
	def decode_ticket(dct):
		return Ticket(dct['number'], dct['date'], dct['destination'], dct['seat'], dct['cost'], dct['status_ticket'])
	
	def load_db(self, file_name):
		try:
			with open(file_name) as f:
				data = f.read()
				self.lst_tickets = json.loads(data, object_hook=DataBase.decode_ticket)
			#print(self.lst_tickets)
		except IOError:
				print("An IOError has occurred!")

	#def add_client_db(self, passport, full_name, ticket_num):
		#for client_dct in lst_clients:
			#if passport not in client:
				#lst_clients.append({ passport : {'full_name': full_name, ticket_number:[ticket_num]} })
			#else:
				#client_dct[ticket_number].append(ticket_num)


	def edit_db(self, ticket, client):
		for ticket_obj in DataBase.lst_tickets:
			if ticket_obj.number == ticket.number:
				ticket_obj.status_ticket = 0
				ticket_obj.buyer = client.passport
				#ticket_obj.client_name = client.full_name

	def save_db(self):
		with open('datebase.json', "w") as write_file:
			json.dump(DataBase.lst_tickets, write_file)
		
	@staticmethod
	def random_date(start, end,  prop):
		return DataBase.time_prop(start, end, '%m/%d/%Y %I:%M %p', prop) 
	
	@staticmethod
	def time_prop(start, end, format_, prop):
		stime = time.mktime(time.strptime(start, format_))
		etime = time.mktime(time.strptime(end, format_))
		ptime = stime + prop * (etime - stime)
		return time.strftime (format_, time.localtime(ptime))

	def automatically_db(self, obj, count=10):
		country = ['Japan, Tokyo', 'France, Paris', 'Russia, Moscow', 'Argentina, Buenos Aires', 'Australia, Canberra',
				'Austria, Vienna', 'Belgium, Brussels', 'Brazil, Brasilia', 'Germany, Berlin', 'China, Pekin',
				'Egypt, Cairo', 'Great Britain, London', 'Hawaii, Honolulu', 'Singapore, Singapore city',
				'South Korea, Seoul', 'Latvia, Riga', 'Italy, Rome', 'Canada, Ottawa', 
				'Chile, Santiago', 'USA, Washington', 'Thailand, Bangkok', 'Poland, Warsaw', 'Indonesia, Jakarta']
		for i in range(count):
			obj.number = random.randint(100000, 999999)
			obj.destination = random.choice(country)
			obj.date = DataBase.random_date("1/1/2020 1:30 PM", "1/1/2021 4:50 AM",  random.random())
			obj.seat = random.randint(10, 999)
			obj.cost = random.randint(300, 850)
			obj.status_ticket = random.randint(0, 1)
			DataBase.lst_tickets.append(obj.__dict__.copy())
			

db = DataBase()		
ticket_obj = Ticket()
db.load_db('datebase.json')

cashdeck_num1 = ElectronicCashDesk()
#client1 = Client('3655122032', 'Ivanov Petr Vasilevich')
client1 = Client(input('Your passport: '), input('Your full name: '))
purchase1 = Purchase()

while exit:
	cashdeck_num1.passport(db.lst_clients, client1.passport, client1.full_name)
	regime = input('\nEnter mode (1 - Buy ticket, 2 - Hand over a ticket, admin - Auto fill database, admin2 - Display database): ')
	if regime == '1':
		purchase1.ticket_obj = cashdeck_num1.sell_a_ticket(db.lst_tickets)
		cashdeck_num1.ticket_obj = purchase1.free_tickets_check(db.lst_tickets)
		if cashdeck_num1.ticket_obj:
			ticket = purchase1.money_request()
			db.edit_db(ticket, client1)
			#db.add_client_db(client1.passport, client1.full_name, ticket)
			db.save_db()
	elif regime == '2':
		pass
	elif regime == 'admin':
		db.automatically_db(ticket_obj)
		db.save_db()
	elif regime == 'admin2':
		db.display_db()

	exit = input("\nDo you want to continue (y / n)? ")
	if exit == 'n':
			break