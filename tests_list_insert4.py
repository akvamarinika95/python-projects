#import list_insert4.py


def my_zip(*args):
	lst = list()
	for i in range(0,len(min(args, key = len))):
		lst.append(tuple(arg[i] for arg in args))
	return lst

#"пузырек":
def bubble_sort(height, new):
	height.append(new) 
	for j in range(0,len(height)):   
		for i in range(0, len(height)-j-1): 
			if height[i] < height[i+1]:
				temp = height[i]
				height[i] = height[i+1]
				height[i+1] = temp
	return height

#вставка с помощью insert()
def insert1(lst, new):
	for i in range(0,len(lst)):
		if lst[i] < new:
			lst.insert(i, new)
			break
		elif new < lst[len(lst)-1]: 
			lst.insert(len(lst), new)
	return lst

# sort()
def libr_sort(lst, new):
	lst.append(new)
	lst.sort(reverse=True)
	return lst

def test(lst, new):
	a = bubble_sort(list(lst), new)
	b = insert1(list(lst), new)
	c = libr_sort(list(lst), new)

	print(a)
	print(b)
	print(c)

	for i, j, k in my_zip (a, b, c):
		if i == j and j == k and k == i:
			print("Test func(lst, new) is OK")
		else:
			print("Test func(lst, new) is Fail")


lst = [170, 165, 163, 163, 160]
new = 162

test(lst, new)