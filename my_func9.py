def multiply(a, b, c):
	return a*b*c

def even(a):
	return (a % 2 == 0)

def my_zip(*args):
	lst = list()
	for i in range(0,len(min(args, key = len))):
		lst.append(tuple(arg[i] for arg in args))
	return lst

def my_map(func, *args):
	lst = list()
	for arg in my_zip(*args):
		#print(arg)
		lst.append(func(*arg))
	return lst

def my_filter(func, *args):
	lst = list()
	if func == None:
		for arg in my_zip(*args):
			if arg:
				lst.append(*arg)
		return lst
	else:
		for arg in my_zip(*args):
				if func(*arg):
					lst.append(*arg)
		return lst



#list [comprehensions] & (generators) 
def zip_compr(*args):
	lst = list()
	lst = (tuple(arg[i] for arg in args) for i in range(len(min(args, key=len))))
	return lst

def map_compr(func, *args):
	lst = list()
	lst.append([func(*arg)for arg in my_zip(*args)])
	return lst

def filter_compr(func, *args):
	lst = list()
	if func == None:
		[arg for arg in my_zip(*args) if arg]
		return lst
	else:
		[(lst.append(*arg)) for arg in my_zip(*args) if func(*arg)]
		return lst


lst = [1, 2, 3, 4, 5, 6, 7, 8]
lst2 = [8, 7, 6, 5, 4, 3, 2, 1]
lst3 = [10, 20, 30, 40, 50, 60, 70, 80]

print(list(my_zip(lst, lst2, lst3)))
print(list(zip_compr(lst, lst2, lst3)))

print(list(my_map(multiply, lst, lst2, lst3)))
print(list(map_compr(multiply, lst, lst2, lst3)))

print(list(my_filter(even, lst)))
print(list(filter_compr(even, lst)))



